export * from "./useDialogs"
export { VfDialogContainer, VfSaveDeleteDialog }
import VfDialogContainer from "./VfDialogContainer.vue"
import VfSaveDeleteDialog from "./VfSaveDeleteDialog.vue"
