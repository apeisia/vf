import { DialogController } from "@/vf"
import type { MaybeRef } from "@vueuse/core"
import { inject, unref } from "vue"

export function useDialogController<ResultType, PropsType>(): DialogController<ResultType, PropsType> {
    const controller = inject<MaybeRef<DialogController<ResultType, PropsType>>>("dialog-controller")

    if (!controller) {
        throw new Error("Dialog controller not found")
    }

    return unref(controller)
}
