import { onMounted, onUnmounted } from "vue"

const $subscribedEvents: { [event: EventName]: Subscription[] } = {}

export type SubscriberCallback = (data?: any) => any
export type EventName = string

export interface EventAggregator {
    subscribe(eventName: EventName, callback: SubscriberCallback): Subscription

    publish(eventName: EventName, data?: any): void
}

export function useEventAggregator(): EventAggregator {
    function subscribe(eventName: EventName, callback: SubscriberCallback): Subscription {
        const subscription: Subscription = {
            callback,
            dispose() {
                const idx = $subscribedEvents[eventName].indexOf(subscription)

                if (idx) {
                    $subscribedEvents[eventName].splice(idx, 1)
                }
            },
        }

        $subscribedEvents[eventName] ??= []
        $subscribedEvents[eventName].push(subscription)

        return subscription
    }

    function publish(event: EventName, data?: any) {
        const subscribers = $subscribedEvents[event] ?? []
        subscribers.forEach(subscriber => subscriber.callback(data))
    }

    return {
        subscribe,
        publish,
    }
}

export interface Subscription {
    callback: SubscriberCallback

    dispose(): void
}

export function createSubscriptionBag(subscribes: [EventName, SubscriberCallback][] = []) {
    const subscriptions: Subscription[] = []
    const eventAggregator = useEventAggregator()

    onMounted(() => {
        subscribes.forEach(([event, callback]) => subscribe(event, callback))
    })

    onUnmounted(() => {
        unsubscribeAll()
    })

    function unsubscribeAll() {
        subscriptions.forEach(subscription => subscription.dispose())
        subscriptions.splice(0, subscriptions.length)
    }

    function subscribe(eventName: EventName, callback: SubscriberCallback) {
        subscriptions.push(eventAggregator.subscribe(eventName, callback))
    }

    return {
        subscriptions,
        unsubscribeAll,
        subscribe,
    }
}
