import { useHttpClient } from "@/vf"
import { ref, type Ref } from "vue"
import type { Router } from "vue-router"
import { vfConfig } from "../config/VfConfig"

const appConfig: Ref<AppConfig | null> = ref(null)
let _router: Router | null = null

async function refreshAppConfig<T extends AppConfig = AppConfig>() {
    if (_router) {
        return await loadInitialAppConfig<T>(_router)
    }
    throw Error("Cannot refresh app config without calling loadInitialAppConfig with a router first.")
}

async function loadInitialAppConfig<T extends AppConfig = AppConfig>(router: Router) {
    const httpClient = useHttpClient()
    let newAppConfig: T | null
    try {
        newAppConfig = (await httpClient.get<T>("/_app_config.json")).data
    } catch (e) {
        newAppConfig = null
    }

    _router = router

    vfConfig.reconfigureRoutes?.(router, newAppConfig?.routes ?? [])
    appConfig.value = newAppConfig
    ;(window as any).appConfig = newAppConfig

    return appConfig as Ref<T>
}

export function useAppConfig<T extends AppConfig = AppConfig>() {
    if (appConfig.value === null) {
        // somehow, in some cases (???), the appConfig variable is lost at hot reload
        appConfig.value = (window as any).appConfig
    }

    const customAppConfig = appConfig as Ref<T>

    return {
        appConfig: customAppConfig,
        refreshAppConfig: refreshAppConfig<T>,
        loadInitialAppConfig: loadInitialAppConfig<T>,
    }
}

export interface AppConfig {
    accesses?: any
    access: any
    account: any
    accountHierarchy: any
    languages?: { iso: string; name: string }[]
    login: any
    menu: AppConfigMenuNode[]
    requiredConditions?: any
    routes: any
    canManageFaq?: boolean
    switchFrom?: any
    isHeadJudge?: boolean
    panels?: { id: string; number: number }[]
}

export interface AppConfigMenuNode {
    id: string
    label: string
    params: Record<string, string>
    settings: Record<string, any>

    // node has either "children" or "route"
    children?: AppConfigMenuNode[]
    route?: string
}
