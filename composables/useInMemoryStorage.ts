import { defineStore } from "pinia"

class MemoryStorage implements Storage {
    [name: string]: any

    get length(): number {
        return Object.keys(this).length
    }

    clear(): void {
        for (const key in Object.keys(this)) {
            this.removeItem(key)
        }
    }

    getItem(key: string): string | null {
        return this[key]
    }

    key(index: number): string | null {
        return Object.keys(this)[index]
    }

    removeItem(key: string): void {
        delete this[key]
    }

    setItem(key: string, value: string): void {
        this[key] = String(value)
    }
}

export const inMemorySessionStorage = defineStore("in-memory-session-storage", () => {
    return {
        storage: new MemoryStorage(),
    }
})
export const inMemoryLocalStorage = defineStore("in-memory-session-storage", () => {
    return {
        storage: new MemoryStorage(),
    }
})
