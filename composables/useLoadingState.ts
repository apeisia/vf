import { reactive, ref } from "vue"

export interface LoadingState {
    state: boolean
    while: <T>(fn: () => Promise<T>) => Promise<void>
}

export function useLoadingState(): LoadingState {
    const state = ref(false)

    return reactive({
        state,
        while: async <T>(fn: () => Promise<T>) => {
            state.value = true
            try {
                await fn()
            } finally {
                state.value = false
            }
        },
    })
}
