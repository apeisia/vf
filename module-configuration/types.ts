import type { Entity } from "@/vf"
import type { Component, Ref } from "vue"

export interface CrudViewModelInterface {
    component?: string | Component
    props?: any
    mode?: null | "dialog" | "in-table" | "router"
    rowId?: string
}

export interface NormalizedCrudViewModelInterface {
    component?: string | Component
    /**
     * Gets appended to the route prefix when in router mode. This is needed because the component name
     * for "new" is "edit".
     */
    routePart?: string
    props?: any
    mode: null | "dialog" | "in-table" | "router"
    highlightKey?: string
    rowId?: string | null
    entity?: Entity
}

export type ListFieldType =
    | "string"
    | "integer"
    | "currency"
    | "datetime"
    | "datetimeshort"
    | "date"
    | "bool"
    | "html"
    | "vnode"
    | "multi-row"

export interface ListFieldGroupConfiguration {
    name: string
    /** Human-readable title of the field group, which is displayed in the table. Can be a translation key */
    title: string
    /**
     * Used to determine which fields are grouped together. All cells that share a highlightKey are highlighted
     * at the same time. Defaults to the value of `name`.
     */
    highlightKey?: string
    fields: ListFieldConfiguration[]
    /**
     * After how many rows should a multi row in this group get collapsed? A "more" button will be renderer, spanning
     * the whole field group, which will expand all multi rows when clicked.
     */
    collapseMultiRowsAfter?: number
}

export interface NormalizedListFieldGroupConfiguration {
    name: string
    title: string
    highlightKey: string
    fields: ListFieldConfiguration[]
    collapseMultiRowsAfter: number
}

export interface ListFieldConfiguration {
    name: string
    title: string
    /** Format of the output. If none of these seem suitable, provide a getter instead. */
    type: ListFieldType
    sortField?: string
    getter?: (entity: Entity) => any
    thClass?: () => string
    tdClass?: (entity: Entity) => string
    defaultHidden?: boolean
}

export interface ListFilterFieldConfiguration {
    name: string
    field: string
    items: { name: string; value: string }[]
}

export type ViewModelName = string
export type PageConfigFn = (entity?: Entity) => ViewModelName | CrudViewModelInterface | Component
export type PageConfig = null | PageConfigFn | ViewModelName | CrudViewModelInterface | Component

export type ButtonVisibility = boolean | (() => boolean)
export type NormalizedButtonVisibility = () => boolean
export type ButtonVisibilityWithEntity = boolean | ((entity: Entity) => boolean)
export type NormalizedButtonVisibilityWithEntity = (entity: Entity) => boolean

export type ListOnClickFn = (
    entity: Entity,
    field: string,
    fieldGroup: NormalizedListFieldGroupConfiguration | undefined,
    multiRowIndex: number | undefined,
) => ViewModelName | CrudViewModelInterface | Component | "edit" | "show" | null | void

export type ListOnClick = "edit" | "show" | null | ListOnClickFn

export type RequestParams = Record<string, string | string[]>

/** The parameters that are used to filter a list (ListRequestParams.filter) */
export type ListFilter = Record<string, string | number | string[]>

/** The query parameters that are passed to the get request of a list */
export type ListRequestParams = {
    search: string
    itemsPerPage: number
    sort: string | undefined
    direction: "asc" | "desc"
    page: number
    filter: ListFilter
    [key: string]: string | string[] | number | ListFilter | undefined
}

/** The request url and params that are used to make a list request */
export type ListRequest = {
    url: string
    params: ListRequestParams
}

export interface ModuleConfigurationOptions {
    safeDeleteDialog?: {
        /** The value that must be entered to delete the entity */
        confirmationProperty?: null | ((entity: Entity) => string)

        /** Enable or disable the safe delete dialog, will be a yes / no dialog if set to false */
        active?: boolean

        /** The prompt that is shown to the user, gets the entity as only parameter */
        prompt?: (entity: Entity) => string
    }

    // defaults for list, show, edit, new
    newButton?: NormalizedButtonVisibility
    showButton?: ButtonVisibility
    editButton?: ButtonVisibilityWithEntity
    deleteButton?: ButtonVisibilityWithEntity
    buttonColumn?: ButtonVisibility
    backButton?: ButtonVisibility

    list?: {
        /** Used to globally identify this list when storing the list state */
        id?: () => string

        /** Defaults to "show", null means no action */
        onClick?: ListOnClick
        rowClickable?: (entity: Entity) => boolean

        /** How many items should be loaded in a list by default? */
        defaultItemsPerPage?: number

        fields?: ListFieldConfiguration[]
        fieldGroups?: ListFieldGroupConfiguration[]
        filterFields?: ListFilterFieldConfiguration[]

        showFieldGroupsHeadRow?: boolean
        showOpenRowAsTab?: boolean

        /** Default order field. Takes the "name" property of a field defined in "fields". */
        orderField?: string | null
        orderDirection?: "asc" | "desc"

        // button visibilities
        newButton?: ButtonVisibility // show "new" Button in list toolbar
        showButton?: ButtonVisibilityWithEntity // show "show" button in list item
        editButton?: ButtonVisibilityWithEntity // show "edit" button in list item
        deleteButton?: ButtonVisibilityWithEntity // show "delete" button in list item
        buttonColumn?: ButtonVisibility // show button column. used to hide the entire column if not needed

        /** Additional parameters for the list http request */
        requestParams?: Ref<RequestParams>

        trClass?: (entity: Entity) => string[] | string

        fieldsCustomizable?: boolean

        defaultFilter?: () => ListFilter
        onRequest?: (request: ListRequest) => ListRequest
    }

    mode?: null | "dialog" | "in-table" | "router"
    edit?:
        | PageConfig
        | {
              page?: PageConfig
              deleteButton?: ButtonVisibilityWithEntity
              backButton?: ButtonVisibilityWithEntity
          }
    show?:
        | PageConfig
        | {
              page?: PageConfig
              editButton?: ButtonVisibilityWithEntity
              deleteButton?: ButtonVisibilityWithEntity
              backButton?: ButtonVisibility
          }
    new?:
        | PageConfig
        | {
              page?: PageConfig
              backButton?: ButtonVisibility
          }
    resolveComponent?: (cvm: NormalizedCrudViewModelInterface, entity: Entity) => Component

    /** Gets prepended to all api urls */
    apiBase?: () => string

    /** Gets the parameters that should be passed to all routes */
    routeParams?: (entity?: Entity) => object

    /** Get the REST url for the given entity. By default, this is automatically built via the apiBase configuration */
    entityPath?: (entity?: any) => string

    /** What happens after an entity was saved? null means no action */
    afterSaveAction?: AfterSaveActionValues

    /** What happens after an entity was deleted? null means no action */
    afterDeleteAction?: null | "auto" | "close" | "navigate-list" | ((entity: Entity) => any)

    /** The path that should be used to determine default components paths */
    routePrefix?: () => string

    /**
     * Used for headings, default confirmations dialogs etc. If provided a string it will be used for both plural and
     * singular. Entity will try to use the objects "name", "title", "subject" or "id" field.
     */
    readableName?:
        | string
        | {
              plural?: string
              singular?: string
              entity?: (entity: Entity) => string
          }
}

export interface ModuleConfiguration {
    safeDeleteDialog: {
        confirmationProperty: (entity: Entity) => string
        active: boolean
        prompt: (entity: Entity) => string
    }

    list: {
        id: () => string
        onClick: (
            entity: Entity,
            field: string,
            fieldGroup: NormalizedListFieldGroupConfiguration | undefined,
            multiRowIndex: number | undefined,
        ) => NormalizedCrudViewModelInterface | null

        rowClickable: (entity: Entity) => boolean
        defaultItemsPerPage: number
        fieldGroups: NormalizedListFieldGroupConfiguration[]
        filterFields: ListFilterFieldConfiguration[]
        showFieldGroupsHeadRow: boolean
        showOpenRowAsTab: boolean
        orderField: string | null
        orderDirection: "asc" | "desc"
        newButton: NormalizedButtonVisibility
        showButton: NormalizedButtonVisibilityWithEntity
        editButton: NormalizedButtonVisibilityWithEntity
        deleteButton: NormalizedButtonVisibilityWithEntity
        buttonColumn: NormalizedButtonVisibility
        requestParams: Ref<RequestParams>
        trClass?: (entity: Entity) => Record<string, true>
        fieldsCustomizable: boolean
        /** The default filter if there is no filter in the session storage */
        defaultFilter: () => ListFilter
        onRequest: (request: ListRequest) => ListRequest
    }

    edit: {
        page: (entity: Entity) => NormalizedCrudViewModelInterface
        deleteButton: NormalizedButtonVisibilityWithEntity
        backButton: NormalizedButtonVisibility
    }

    show: {
        page: (entity: Entity) => NormalizedCrudViewModelInterface
        editButton: NormalizedButtonVisibilityWithEntity
        deleteButton: NormalizedButtonVisibilityWithEntity
        backButton: NormalizedButtonVisibility
    }

    new: {
        page: () => NormalizedCrudViewModelInterface
        backButton: () => boolean
    }

    resolveComponent: (cvm: NormalizedCrudViewModelInterface, entity: Entity) => Component | null

    apiBase: () => string
    routeParams: (entity?: Entity) => any
    entityPath: (entityId: string | number | Entity | undefined) => string
    afterSaveAction: AfterSaveActionValues
    afterDeleteAction: null | "auto" | "close" | "navigate-list" | ((entity: Entity) => any)
    routePrefix: () => string | undefined
    readableName: {
        plural: string
        singular: string
        entity: (entity: Entity) => string
    }
}

export type AfterSaveActionValues =
    | null
    | "auto"
    | "close" // todo remove
    | "close-dialog" // todo remove
    | "navigate-list"
    | "navigate-show"
    | ((entity: Entity) => any)
