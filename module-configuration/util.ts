import type { Component } from "vue"

export function isVueComponent(component: any): component is Component {
    return typeof component === "object" && component !== null && "setup" in component
}
