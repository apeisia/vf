import type { MaybeRef } from "@vueuse/core"
import { computed, unref } from "vue"

export const STRENGTH = {
    VERY_WEAK: 0,
    WEAK: 1,
    MEDIUM: 2,
    GOOD: 3,
    STRONG: 4,
    VERY_STRONG: 5,
}

export const LIMIT_FACTOR = {
    LENGTH: "length",
    MISSING_SYMBOLS: "symbols",
    MISSING_CASE: "capitals_minuscules",
    MISSING_NUMBERS: "numbers",
}

export interface Analysis {
    input: string
    length: number
    numbers: number
    minuscules: number
    capitals: number
    symbols: number
    unique: number
}

export type LimitFactor = (typeof LIMIT_FACTOR)[keyof typeof LIMIT_FACTOR]
export type Strength = (typeof STRENGTH)[keyof typeof STRENGTH]

export interface AnalysisResult {
    strength: Strength
    score: number
    limitedBy: LimitFactor[]
    limitFactors: Record<LimitFactor, number>
    input: string
    analysis: Analysis
}

function limitResultTo(
    result: AnalysisResult,
    factor: (typeof LIMIT_FACTOR)[keyof typeof LIMIT_FACTOR],
    maxStrength: (typeof STRENGTH)[keyof typeof STRENGTH],
) {
    result.limitedBy.push(factor)
    result.limitFactors[factor] = maxStrength
}

export function usePasswordValuation(password: MaybeRef<string>) {
    /**
     * Analyses the given password and returns an object containing information about how the strength is calculated.
     */
    function analyse(rawPassword: string): Analysis {
        const appearedCharacters: string[] = []
        const result: Analysis = {
            input: rawPassword,
            length: rawPassword.length,
            numbers: 0,
            minuscules: 0,
            capitals: 0,
            symbols: 0,
            unique: 0,
        }

        for (let i = 0; i < rawPassword.length; ++i) {
            const character = rawPassword[i]

            if ("abcdefghijklmnopqrstuvwxyz".indexOf(character) >= 0) {
                result.minuscules++
            } else if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(character) >= 0) {
                result.capitals++
            } else if ("01213456789".indexOf(character) >= 0) {
                result.numbers++
            } else {
                result.symbols++
            }

            if (!appearedCharacters.includes(character)) {
                appearedCharacters.push(character)
                result.unique++
            }
        }

        return result
    }

    function valuate(rawPassword: string) {
        const analysis = analyse(rawPassword)
        let maxStrength = 5
        let score = 0
        const result: AnalysisResult = {
            strength: STRENGTH.VERY_WEAK,
            score: 0,
            limitedBy: [],
            limitFactors: {},
            input: analysis.input,
            analysis: analysis,
        }
        const symbolPercentage = analysis.symbols / analysis.length

        // score by the amount of characters included in the password
        score += analysis.symbols * 3
        score += analysis.capitals * 2
        score += analysis.minuscules * 1.5
        score += analysis.numbers

        // punish repeating characters
        score *= analysis.length / analysis.unique

        // punish missing characters by limiting the maximum strength
        if (analysis.symbols === 0) {
            maxStrength--
            limitResultTo(result, LIMIT_FACTOR.MISSING_SYMBOLS, maxStrength)
        }

        if (symbolPercentage < 0.5) {
            // allow missing capitals, minuscules and numbers if we have a good percentage of symbols
            if (analysis.capitals === 0 || analysis.minuscules === 0) {
                maxStrength--
                limitResultTo(result, LIMIT_FACTOR.MISSING_CASE, maxStrength)
            }

            if (analysis.numbers === 0) {
                maxStrength--
                limitResultTo(result, LIMIT_FACTOR.MISSING_NUMBERS, maxStrength)
            }
        }

        // punish very short passwords
        if (analysis.length < 4) {
            maxStrength = STRENGTH.VERY_WEAK
            limitResultTo(result, LIMIT_FACTOR.LENGTH, maxStrength)
        }

        // valuate the score
        result.score = score
        result.strength = STRENGTH.VERY_WEAK

        if (result.score >= 10) {
            result.strength = STRENGTH.WEAK
        }

        if (result.score >= 15) {
            result.strength = STRENGTH.MEDIUM
        }

        if (result.score >= 20) {
            result.strength = STRENGTH.GOOD
        }

        if (result.score >= 25) {
            result.strength = STRENGTH.STRONG
        }

        if (result.score >= 35) {
            result.strength = STRENGTH.VERY_STRONG
        }

        // limit the result strength to the previously determined maximum
        if (result.strength > maxStrength) {
            result.strength = maxStrength
        }

        return result
    }

    const valuation = computed(() => valuate(unref(password)))

    return {
        strength: computed(() => valuation.value.strength),
        score: computed(() => valuation.value.score),
        limitedBy: computed(() => valuation.value.limitedBy),
        limitFactors: computed(() => valuation.value.limitFactors),
    }
}
