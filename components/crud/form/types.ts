import type { FormModel } from "@/vf"
import type { FormKitSchemaNode } from "@formkit/core"
import type { Ref, ShallowReactive } from "vue"

export interface VfFormInject {
    schema: Ref<FormKitSchemaNode[]>
    formModel: ShallowReactive<FormModel>
    renderedList: Ref<string[]>
}

export type FormErrors = Record<string, string[]>
