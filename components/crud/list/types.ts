import type { Entity,ListFieldConfiguration,ListFilter,NormalizedListFieldGroupConfiguration } from "@/vf"

export type ListStateHooks = {
    refresh: (background?: boolean) => Promise<void>
    columnClicked: (
        item: any,
        field: ListFieldConfiguration | "button:show",
        fieldGroup: NormalizedListFieldGroupConfiguration | undefined,
        multiRowIndex: number | undefined,
    ) => void
    deleteEntity: (item: any) => void
    editEntity: (item: any) => void
    resetCustomizeFields: (overrideCurrentSettings?: boolean, forceAllTrue?: boolean) => void
}

export interface ListState {
    orderField: string | null
    orderDirection: "asc" | "desc"
    page: number
    filter: ListFilter
    searchValue: string
    itemsPerPage: number
    totalItems: number
    pageCount: number
    list: any[]
    pagination: number[]
    customizeFields: {
        [groupName: string]: {
            enabled: boolean
            fields: {
                [fieldName: string]: {
                    enabled: boolean
                }
            }
        }
    }
    loading: boolean
    columnClicked: (
        item: Entity,
        field: ListFieldConfiguration | "button:show",
        fieldGroup?: NormalizedListFieldGroupConfiguration,
        multiRowIndex?: number,
    ) => void
    deleteEntity: (item: any) => void
    editEntity: (item: any) => void
    refresh: (background?: boolean) => Promise<void>
    resetCustomizeFields: (overrideCurrentSettings: boolean, forceAllTrue?: boolean) => void
    hook: (hooks: ListStateHooks) => void
}

// this does not work probably because of a too old vue version
// export interface VfComposeInlineTableProps {
//     config?: ModuleConfiguration
//     inlineTable: InlineTable
//     rowId: string
// }
//
// export interface VfListComposeProps extends VfComposeInlineTableProps {
//     colspan?: number
// }
