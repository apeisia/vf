import type { ListState } from "@/vf"

export class ListController {
    private onRefresh: (() => void) | undefined = undefined

    refresh() {
        this.onRefresh?.()
    }

    attach(listState: ListState) {
        this.onRefresh = () => listState.refresh(false)
    }
}
