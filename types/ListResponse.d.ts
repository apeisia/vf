import { Entity } from "./Entity"

export interface ListResponse<ItemType extends Entity = Entity> {
    pageCount: number
    page: number
    itemsPerPage: number
    list: ItemType[]
    totalItems: number
}
