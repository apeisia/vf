import { Entity } from "./Entity"

export interface EntityEvent<T = Entity> {
    entity: T
    cancel: boolean
}
