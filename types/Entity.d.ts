export type Entity = {
    id?: string
    [key: string]: any
}

export type FormModel = Record<string, any>
