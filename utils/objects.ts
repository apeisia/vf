export function deepCopy(obj: any) {
    if (obj === undefined) {
        return undefined
    }

    return JSON.parse(JSON.stringify(obj))
}
