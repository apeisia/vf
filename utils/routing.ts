export function augmentRouteParams(base: { [key: string]: any }, item: { id?: any }) {
    const params = { ...base }

    if (item && item.id) {
        params.id = item.id
    }

    return params
}
